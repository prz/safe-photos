/*
 * Copyright 2020 cyberfikcja.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

plugins {
    id("com.android.application")
    kotlin("android")
    kotlin("android.extensions")
    kotlin("kapt")

    id("dagger.hilt.android.plugin")
    id("kotlin-android")
}

android {
    compileSdkVersion(Versions.COMPILE_SDK)

    defaultConfig {
        applicationId = "net.cyberfikcja.safephotos"
        minSdkVersion(Versions.MIN_SDK)
        targetSdkVersion(Versions.TARGET_SDK)
        versionCode = Versions.versionCode
        versionName = Versions.versionName

        javaCompileOptions {
            annotationProcessorOptions {
                arguments["room.incremental"] = "true"
            }
        }
    }

    buildTypes {
        getByName("debug") {
            versionNameSuffix = "-debug"
        }
    }

    buildFeatures {
        // Determines whether to generate a BuildConfig class.
        // buildConfig = true
        // Determines whether to support View Binding.
        // Note that the viewBinding.enabled property is now deprecated.
        viewBinding = true
        // Determines whether to support Data Binding.
        // Note that the dataBinding.enabled property is now deprecated.
        dataBinding = true
        // Determines whether to generate binder classes for your AIDL files.
        // aidl = true
        // Determines whether to support RenderScript.
        // renderScript = true
        // Determines whether to support injecting custom variables into the module’s R class.
        // resValues = true
        // Determines whether to support shader AOT compilation.
        // shaders = true
    }

    lintOptions {
        isCheckGeneratedSources = false
        isCheckReleaseBuilds = false
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    kotlinOptions {
        val options = this as org.jetbrains.kotlin.gradle.dsl.KotlinJvmOptions
        options.jvmTarget = "1.8"
        options.freeCompilerArgs = listOf("-Xallow-result-return-type")
    }
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))

    api(platform(project(":depconstraints")))
    kapt(platform(project(":depconstraints")))

    // User Interface
    implementation(Libraries.ACTIVITY_KTX)
    implementation(Libraries.APPCOMPAT)
    implementation(Libraries.CARDVIEW)
    implementation(Libraries.CONSTRAINT_LAYOUT)
    implementation(Libraries.CORE_KTX)
    implementation(Libraries.DRAWER_LAYOUT)
    implementation(Libraries.FRAGMENT_KTX)
    implementation(Libraries.MATERIAL)

    // Arch Components
    kapt(Libraries.LIFECYCLE_COMPILER)
    implementation(Libraries.LIFECYCLE_LIVE_DATA_KTX)
    testImplementation(Libraries.ARCH_TESTING)

    // Room
    kapt(Libraries.ROOM_COMPILER)
    implementation(Libraries.ROOM_KTX)
    implementation(Libraries.ROOM_RUNTIME)
    testImplementation(Libraries.ROOM_KTX)
    testImplementation(Libraries.ROOM_RUNTIME)

    // Hilt
    kapt(Libraries.ANDROIDX_HILT_COMPILER)
    kapt(Libraries.HILT_COMPILER)
    implementation(Libraries.HILT_ANDROID)
    implementation(Libraries.HILT_VIEWMODEL)
    kaptAndroidTest(Libraries.ANDROIDX_HILT_COMPILER)
    kaptAndroidTest(Libraries.HILT_COMPILER)
    androidTestImplementation(Libraries.HILT_TESTING)

    // Glide
    kapt(Libraries.GLIDE_COMPILER)
    implementation(Libraries.GLIDE)

    // Kotlin
    implementation(Libraries.KOTLIN_STDLIB)

    // GSON - lock gson version as it might be used by other deps
    implementation(Libraries.GSON)
    implementation(Libraries.TIMBER)
    implementation(Libraries.RETROFIT)
    implementation(Libraries.RETROFIT_CONVERTER_GSON)

    // Android Security Crypto
    implementation(Libraries.SECURITY_CRYPTO)

    // Paging
    implementation(Libraries.PAGING_RUNTIME_KTX)
    implementation(Libraries.PAGING_COMMON_KTX)

    // Instrumentation tests
    androidTestImplementation(Libraries.ESPRESSO_CORE)
    androidTestImplementation(Libraries.ESPRESSO_CONTRIB)
    androidTestImplementation(Libraries.EXT_JUNIT)
    androidTestImplementation(Libraries.RUNNER)
    androidTestImplementation(Libraries.RULES)

    // Unit tests
    testImplementation(Libraries.JUNIT)
    testImplementation(Libraries.MOCKITO_CORE)
    testImplementation(Libraries.MOCKITO_KOTLIN)
    testImplementation(Libraries.HAMCREST)
}
