/*
 * Copyright 2020 Cyberfikcja
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.cyberfikcja.safephotos.domain

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.mapNotNull
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.onEmpty
import net.cyberfikcja.safephotos.core.FlowUseCase
import net.cyberfikcja.safephotos.core.IoDispatcher
import timber.log.Timber
import javax.inject.Inject

class LoadUserSessionUseCase @Inject constructor(
    @IoDispatcher dispatcher: CoroutineDispatcher,
    private val userRepository: UserRepository
) : FlowUseCase<Unit, UserSession>(dispatcher) {
    override fun execute(parameters: Unit): Flow<Result<UserSession>> =
        userRepository.userSession
            .onEach { Timber.d("result [$it]") }
            .mapNotNull { Result.success(it) }
            .onEmpty { Result.success(UserSession.InActive) }
}
