/*
 * Copyright 2020 Cyberfikcja
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.cyberfikcja.safephotos.domain

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.mapNotNull
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.onEmpty
import net.cyberfikcja.safephotos.core.FlowUseCase
import net.cyberfikcja.safephotos.core.IoDispatcher
import net.cyberfikcja.safephotos.data.model.Password
import timber.log.Timber
import javax.inject.Inject

class LoadUserPasswordUseCase @Inject constructor(
    @IoDispatcher dispatcher: CoroutineDispatcher,
    private val loadUserSessionUseCase: LoadUserSessionUseCase
) : FlowUseCase<Unit, Password>(dispatcher) {
    override fun execute(parameters: Unit): Flow<Result<Password>> =
        loadUserSessionUseCase(Unit)
            .onEach { Timber.d("result [$it]") }
            .map { it.getOrThrow() }
            .mapNotNull {
                when (it) {
                    is UserSession.Active -> it.password
                    UserSession.InActive -> null
                }
            }
            .onEmpty { Result.failure<Password>(UnauthorizedException("User session inactive")) }
            .map { Result.success(it) }

    class UnauthorizedException(override val message: String?) : Throwable()
}
