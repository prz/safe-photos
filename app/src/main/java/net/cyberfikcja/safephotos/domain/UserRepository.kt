/*
 * Copyright 2020 Cyberfikcja
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.cyberfikcja.safephotos.domain

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.asFlow
import androidx.lifecycle.asLiveData
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.shareIn
import net.cyberfikcja.safephotos.data.model.Password
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

interface UserDataSource {
    suspend fun save(password: Password): Flow<UserSession>
    suspend fun flush()
}

sealed class UserSession {
    data class Active(val password: Password) : UserSession()
    object InActive : UserSession()
}


@Singleton
class UserRepository @Inject constructor(
) : UserDataSource {

    private val _userSession = MutableStateFlow<UserSession>(UserSession.InActive)
    val userSession: Flow<UserSession> = _userSession.asStateFlow()

    override suspend fun save(password: Password) : Flow<UserSession> {
        Timber.d("save [$password]")
        _userSession.emit(UserSession.Active(password))
        return userSession
    }

    override suspend fun flush() {
        Timber.d("flush")
        _userSession.emit(UserSession.InActive)
    }

}
