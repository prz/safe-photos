/*
 * Copyright 2020 cyberfikcja.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.cyberfikcja.safephotos.domain

import android.graphics.Bitmap
import kotlinx.coroutines.CoroutineDispatcher
import net.cyberfikcja.safephotos.core.IoDispatcher
import net.cyberfikcja.safephotos.core.UseCase
import net.cyberfikcja.safephotos.data.EncryptedPhoto
import net.cyberfikcja.safephotos.data.EncryptedPhotosDatabase
import net.cyberfikcja.safephotos.data.model.CryptoContent
import net.cyberfikcja.safephotos.data.model.Password
import net.cyberfikcja.safephotos.data.model.asByteContent
import net.cyberfikcja.safephotos.domain.EncryptAndStoreBitmapUseCase.BitmapPasswordData
import java.io.ByteArrayOutputStream
import javax.inject.Inject

class EncryptAndStoreBitmapUseCase @Inject constructor(
    @IoDispatcher private val dispatcher: CoroutineDispatcher,
    private val encryptedPhotosDatabase: EncryptedPhotosDatabase,
    private val encryptOrDecryptUseCase: EncryptOrDecryptUseCase
) : UseCase<BitmapPasswordData, BitmapResultData>(dispatcher) {
    override suspend fun execute(input: BitmapPasswordData): BitmapResultData {
        // convert bitmap to byte[]
        val content = with(ByteArrayOutputStream()) {
            input.bitmap.compress(Bitmap.CompressFormat.JPEG, 50, this)
            toByteArray()
        }.asByteContent()

        // encrypt bitmap bytes
        val data = CryptoContent(
            password = input.password,
            content = content,
            mode = CryptoContent.Mode.ENCRYPT
        )
        val encryptedContent = encryptOrDecryptUseCase(data).getOrThrow()

        // store encrypted bitmap bytes in the database
        val encryptedPhoto = EncryptedPhoto(
            content = encryptedContent.bytes
        )
        val id = encryptedPhotosDatabase.encryptedPhotos().insert(encryptedPhoto)

        // return _unencrypted_ content and ID
        return BitmapResultData(
            id,
            input.bitmap
        )
    }

    data class BitmapPasswordData(val bitmap: Bitmap, val password: Password)
}
