/*
 * Copyright 2020 cyberfikcja.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.cyberfikcja.safephotos.domain

import kotlinx.coroutines.CoroutineDispatcher
import net.cyberfikcja.safephotos.core.IoDispatcher
import net.cyberfikcja.safephotos.core.UseCase
import net.cyberfikcja.safephotos.data.model.ByteContent
import net.cyberfikcja.safephotos.data.model.Password
import net.cyberfikcja.safephotos.domain.GenerateSecretKeyFromPasswordUseCase.SecretPassword
import javax.crypto.SecretKey
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.PBEKeySpec
import javax.crypto.spec.SecretKeySpec
import javax.inject.Inject

class GenerateSecretKeyFromPasswordUseCase @Inject constructor(
    @IoDispatcher private val dispatcher: CoroutineDispatcher
) : UseCase<SecretPassword, SecretKey>(dispatcher) {

    companion object {
        private const val AES_ALGO = "AES"
        private const val SECRET_KEY_ALGO = "PBKDF2WithHmacSHA512"

        private const val ITERATION_COUNT = 10_000
        private const val KEY_SIZE = 256
    }

    override suspend fun execute(input: SecretPassword): SecretKey {
        val encoded = SecretKeyFactory
            .getInstance(SECRET_KEY_ALGO)
            .generateSecret(
                PBEKeySpec(
                    input.value.chars,
                    input.secret.bytes,
                    ITERATION_COUNT,
                    KEY_SIZE
                )
            ).encoded
        return SecretKeySpec(
            encoded,
            AES_ALGO
        )
    }

    data class SecretPassword(
        val value: Password,
        val secret: ByteContent
    )
}
