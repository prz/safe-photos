/*
 * Copyright 2020 Cyberfikcja
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.cyberfikcja.safephotos.domain

import androidx.lifecycle.asFlow
import androidx.lifecycle.liveData
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.mapNotNull
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.single
import net.cyberfikcja.safephotos.core.FlowUseCase
import net.cyberfikcja.safephotos.core.IoDispatcher
import net.cyberfikcja.safephotos.data.EncryptedPhoto
import net.cyberfikcja.safephotos.data.EncryptedPhotosDatabase
import net.cyberfikcja.safephotos.data.model.ByteContent
import net.cyberfikcja.safephotos.data.model.CryptoContent
import net.cyberfikcja.safephotos.data.model.Password
import timber.log.Timber
import javax.inject.Inject

class SavePhotoUseCase @Inject constructor(
    @IoDispatcher private val dispatcher: CoroutineDispatcher,
    private val database: EncryptedPhotosDatabase,
    private val loadPassword: LoadUserPasswordUseCase,
    private val encrypt: EncryptOrDecryptUseCase,
    private val convertToBytes: ConvertBitmapToBytesUseCase,
    private val invalidateLoadedPhotos: InvalidateLoadedPhotos
) : FlowUseCase<PhotoState.Photo, Long>(dispatcher) {
    override fun execute(parameters: PhotoState.Photo): Flow<Result<Long>> {
        return liveData {
            val content = convertToBytes(parameters.bitmap).getOrThrow()
            Timber.d("content size [${content.bytes.size}]")

            loadPassword(Unit)
                .mapNotNull { it.getOrThrow() }
                .onEach { Timber.d("pass $it") }
                .mapNotNull { encrypt(getCryptoContent(it, content)).getOrThrow() }
                .mapNotNull {
                    database.encryptedPhotos().insert(EncryptedPhoto(content = it.bytes)).run {
//                        invalidateLoadedPhotos(Unit).getOrThrow()
                        emit(this)
                    }
                }
                .flowOn(dispatcher)
                .onEach { }
                .single()
        }.asFlow()
            .catch { Result.failure<Long>(it) }
            .map { Result.success(it) }
    }

    private fun getCryptoContent(
        password: Password,
        content: ByteContent
    ) = CryptoContent(
        password = password,
        content = content,
        mode = CryptoContent.Mode.ENCRYPT
    )
}
