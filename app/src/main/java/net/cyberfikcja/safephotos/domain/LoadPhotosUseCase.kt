/*
 * Copyright 2020 Cyberfikcja
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.cyberfikcja.safephotos.domain

import android.graphics.Bitmap
import androidx.paging.ExperimentalPagingApi
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import androidx.paging.insertSeparators
import androidx.paging.map
import androidx.room.InvalidationTracker
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.channelFlow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.mapNotNull
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.zip
import net.cyberfikcja.safephotos.core.FlowUseCase
import net.cyberfikcja.safephotos.core.IoDispatcher
import net.cyberfikcja.safephotos.data.EncryptedPhoto
import net.cyberfikcja.safephotos.data.EncryptedPhotosDatabase
import net.cyberfikcja.safephotos.data.model.ByteContent
import net.cyberfikcja.safephotos.data.model.CryptoContent
import net.cyberfikcja.safephotos.data.model.Password
import timber.log.Timber
import javax.inject.Inject

sealed class PhotoState {
    data class Photo(val id: Long = -1, val bitmap: Bitmap) : PhotoState()
    data class Separator(val head: Boolean) : PhotoState()
}

class LoadPhotosUseCase @Inject constructor(
    @IoDispatcher private val dispatcher: CoroutineDispatcher,
    private val database: EncryptedPhotosDatabase,
    private val loadPassword: LoadUserPasswordUseCase,
    private val decrypt: EncryptOrDecryptUseCase,
    private val convertToBitmap: ConvertBytesToBitmapUseCase
) : FlowUseCase<LoadPhotosUseCase.Params, LoadPhotosUseCase.LoadPhotosAction>(dispatcher) {

    data class Params(val pageSize: Int, val scope: CoroutineScope)

    sealed class LoadPhotosAction {
        data class Content(val pagingData: Flow<PagingData<PhotoState>>) : LoadPhotosAction()
    }

    @ExperimentalPagingApi
    override fun execute(parameters: Params): Flow<Result<LoadPhotosAction>> {
        val password = loadPassword(Unit)
            .map { it.getOrThrow() }
        database.encryptedPhotos().loadPhotos().registerInvalidatedCallback {
            Timber.d("Database invalidated")
        }
        return Pager(
            pagingSourceFactory = { database.encryptedPhotos().loadPhotos() },
            config = PagingConfig(
                pageSize = 8,
                enablePlaceholders = true,
                jumpThreshold = 8,
                initialLoadSize = 8,
                maxSize = 48,
                prefetchDistance = 4
            )
        ).flow
            .zip(password) { pagingData, pass ->
                Timber.d("zipping $pagingData")
                pagingData.map { encrypted ->
                    return@map decrypt(getCryptoContent(pass, encrypted)).map {
                        val id = encrypted.id ?: throw IllegalStateException("null ID")
                        val bitmap = convertToBitmap(it).getOrThrow()
                        PhotoState.Photo(id, bitmap)
                    }.getOrThrow() as PhotoState
                }
            }
            .map {
                it.insertSeparators { before, after ->
                    when {
                        before == null -> PhotoState.Separator(false)
                        after == null -> PhotoState.Separator(true)
                        else -> null
                    }
                }
            }
            .cachedIn(parameters.scope)
            .onEach {
                Timber.d("next result $it")
            }
            .mapNotNull { Result.success(LoadPhotosAction.Content(pagingData = channelFlow { offer(it) })) }
    }

    private fun getCryptoContent(
        password: Password,
        data: EncryptedPhoto
    ): CryptoContent = CryptoContent(
        password = password,
        content = ByteContent(data.content),
        mode = CryptoContent.Mode.DECRYPT
    )
}
