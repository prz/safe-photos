/*
 * Copyright 2020 cyberfikcja.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.cyberfikcja.safephotos.domain

import kotlinx.coroutines.CoroutineDispatcher
import net.cyberfikcja.safephotos.core.IoDispatcher
import net.cyberfikcja.safephotos.core.UseCase
import net.cyberfikcja.safephotos.data.EncryptedPreferencesStorage
import net.cyberfikcja.safephotos.data.model.ByteContent
import net.cyberfikcja.safephotos.data.model.CryptoContent
import net.cyberfikcja.safephotos.data.model.Password
import net.cyberfikcja.safephotos.data.model.asByteContent
import net.cyberfikcja.safephotos.domain.GenerateSecretKeyFromPasswordUseCase.SecretPassword
import timber.log.Timber
import java.security.SecureRandom
import javax.crypto.Cipher
import javax.crypto.SecretKey
import javax.crypto.spec.IvParameterSpec
import javax.inject.Inject
import javax.inject.Provider

class EncryptOrDecryptUseCase @Inject constructor(
    private val generateSecretKeyFromPasswordUseCase: GenerateSecretKeyFromPasswordUseCase,
    private val encryptedPreferencesStorage: EncryptedPreferencesStorage,
    @AesCipher private val cipher: Provider<Cipher>,
    @IoDispatcher private val dispatcher: CoroutineDispatcher
) : UseCase<CryptoContent, ByteContent>(dispatcher) {

    override suspend fun execute(input: CryptoContent): ByteContent {
        Timber.d("input [${input.content.bytes.size}]")
        val secretKey = getSecretKeyBytes(input.password)
        val cipher = cipher.get()
        val mode = when (input.mode) {
            CryptoContent.Mode.ENCRYPT -> Cipher.ENCRYPT_MODE
            CryptoContent.Mode.DECRYPT -> Cipher.DECRYPT_MODE
        }
        ByteArray(16).run {
            SecureRandom().nextBytes(this)
            cipher.init(mode, secretKey, IvParameterSpec(this))
        }
        return cipher.doFinal(input.content.bytes).asByteContent()
    }

    private suspend fun getSecretKeyBytes(
        password: Password
    ): SecretKey {
        return generateSecretKeyFromPasswordUseCase(
            SecretPassword(
                value = password,
                secret = encryptedPreferencesStorage.getOrCreateSecret()
            )
        ).getOrThrow()
    }
}
