/*
 * Copyright 2020 cyberfikcja.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.cyberfikcja.safephotos.core

import androidx.lifecycle.Observer

/**
 * Base framework event content wrapper. Use [SingleEventObserver] for handling events.
 */
class Event<out T>(private val content: T) {
    var consumed: Boolean = false
        private set

    /**
     * Returns `null` if already consumed.
     */
    fun consumeOrNull(): T? {
        return if (consumed) {
            null
        } else {
            consumed = true
            content
        }
    }

    /**
     * Just preview content without consuming.
     */
    fun preview() = content
}

/**
 * An event [Observer] which invokes [onConsumableEvent] only when [Event] was not consumed yet.
 */
class SingleEventObserver<T>(val onConsumableEvent: (T) -> Unit) : Observer<Event<T>> {
    override fun onChanged(event: Event<T>?) {
        event?.consumeOrNull()?.let { onConsumableEvent(it) }
    }
}
