/*
 * Copyright 2020 cyberfikcja.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.cyberfikcja.safephotos.data

import net.cyberfikcja.safephotos.data.model.ByteContent

interface EncryptedPreferencesStorage {
    /**
     * Generates 256 bits secret
     */
    suspend fun getOrCreateSecret(): ByteContent

    companion object {
        internal const val SECRET_SIZE = 8
    }
}
