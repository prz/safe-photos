/*
 * Copyright 2020 cyberfikcja.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.cyberfikcja.safephotos.data

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext

@Module
@InstallIn(ApplicationComponent::class)
object EncryptedPhotosDatabaseModule {
    @Provides
    fun provideEncryptedPhotosDatabase(@ApplicationContext context: Context): EncryptedPhotosDatabase {
        return Room.databaseBuilder(
            context,
            EncryptedPhotosDatabase::class.java,
            "photos-db"
        ).build()
    }

    @Provides
    fun providesEncryptedPhotosDao(appDatabase: EncryptedPhotosDatabase) =
        appDatabase.encryptedPhotos()
}
