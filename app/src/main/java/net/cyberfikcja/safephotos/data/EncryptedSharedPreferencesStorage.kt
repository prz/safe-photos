/*
 * Copyright 2020 cyberfikcja.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.cyberfikcja.safephotos.data

import android.content.Context
import android.util.Base64
import androidx.core.content.edit
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKeys
import dagger.hilt.android.qualifiers.ApplicationContext
import net.cyberfikcja.safephotos.data.EncryptedPreferencesStorage.Companion.SECRET_SIZE
import net.cyberfikcja.safephotos.data.model.ByteContent
import net.cyberfikcja.safephotos.data.model.asByteContent
import java.math.BigInteger
import java.nio.charset.StandardCharsets
import java.security.SecureRandom
import javax.inject.Inject

class EncryptedSharedPreferencesStorage @Inject constructor(
    @ApplicationContext private val context: Context
) : EncryptedPreferencesStorage {

    companion object {
        private const val SHARED_PREFERENCES_NAME = "photo_pref_name"
        private const val KEY_PREFERENCE_KEY = "key"
    }

    private val sharedPreferences by lazy {
        EncryptedSharedPreferences.create(
            SHARED_PREFERENCES_NAME,
            MasterKeys.getOrCreate(MasterKeys.AES256_GCM_SPEC),
            context,
            EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
            EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
        )
    }

    override suspend fun getOrCreateSecret(): ByteContent {
        // try loading secret, if success return decoded
        val loadedSecret = sharedPreferences.getString(KEY_PREFERENCE_KEY, null)
        if (!loadedSecret.isNullOrBlank()) {
            return Base64.decode(loadedSecret, Base64.DEFAULT).asByteContent()
        }

        // otherwise generate random 256-bit secret
        val secret = ByteArray(SECRET_SIZE)
        val randomString: String = BigInteger(130, SecureRandom()).toString(32)
        System.arraycopy(
            randomString.toByteArray(StandardCharsets.UTF_8),
            0,
            secret,
            0,
            SECRET_SIZE
        )

        // encode and save secret to encrypted preferences storage
        val secretEncoded = Base64.encodeToString(secret, Base64.DEFAULT)
        sharedPreferences.edit {
            putString(KEY_PREFERENCE_KEY, secretEncoded)
        }

        return secret.asByteContent()
    }
}
