/*
 * Copyright 2020 cyberfikcja.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.cyberfikcja.safephotos.data.model

import java.util.concurrent.atomic.AtomicBoolean

data class Password(val chars: CharArray) {
    private val cleared = AtomicBoolean(false)

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Password

        if (!chars.contentEquals(other.chars)) return false

        return true
    }

    override fun hashCode(): Int {
        return chars.contentHashCode()
    }

//    override fun toString(): String = "Password(cleared [${cleared.get()}])"

    @Synchronized
    fun clear() {
        if (cleared.compareAndSet(false, true)) {
            chars.fill('0')
        }
    }
}

fun CharArray.asPassword(): Password =
    Password(this)
