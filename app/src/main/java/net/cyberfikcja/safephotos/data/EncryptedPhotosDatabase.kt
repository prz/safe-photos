/*
 * Copyright 2020 cyberfikcja.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.cyberfikcja.safephotos.data

import androidx.paging.PagingSource
import androidx.room.ColumnInfo
import androidx.room.Dao
import androidx.room.Database
import androidx.room.Delete
import androidx.room.Entity
import androidx.room.Index
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.PrimaryKey
import androidx.room.Query
import androidx.room.RoomDatabase
import java.time.OffsetDateTime

@Database(entities = [EncryptedPhoto::class], version = 1)
abstract class EncryptedPhotosDatabase : RoomDatabase() {
    abstract fun encryptedPhotos(): EncryptedPhotosDao
}

@Entity(tableName = "encrypted_photos", indices = [Index("id", "timestamp", unique = true)])
data class EncryptedPhoto(
    @PrimaryKey(autoGenerate = true) var id: Long? = null,
    @ColumnInfo(typeAffinity = ColumnInfo.BLOB) val content: ByteArray,
    val timestamp: Long = OffsetDateTime.now().toEpochSecond()
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as EncryptedPhoto

        if (id != other.id) return false
        if (!content.contentEquals(other.content)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + content.contentHashCode()
        return result
    }
}

@Dao
interface EncryptedPhotosDao {
    @Query("SELECT * FROM encrypted_photos ORDER BY id ASC")
    fun loadPhotos(): PagingSource<Int, EncryptedPhoto>

    @Query("SELECT * FROM encrypted_photos WHERE id IN (:id)")
    fun getById(id: Long): EncryptedPhoto?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(encryptedPhoto: EncryptedPhoto): Long

    @Delete
    fun delete(encryptedPhoto: EncryptedPhoto)
}
