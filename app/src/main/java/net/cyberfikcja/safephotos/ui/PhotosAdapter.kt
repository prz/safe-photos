/*
 * Copyright 2020 cyberfikcja.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.cyberfikcja.safephotos.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade
import kotlinx.coroutines.CoroutineDispatcher
import net.cyberfikcja.safephotos.R
import net.cyberfikcja.safephotos.core.IoDispatcher
import net.cyberfikcja.safephotos.core.MainDispatcher
import net.cyberfikcja.safephotos.databinding.RowPhotoSepartorBinding
import net.cyberfikcja.safephotos.databinding.RowPhotoThumbBinding
import javax.inject.Inject

class PhotosAdapter @Inject constructor(
    @MainDispatcher private val mainDispatcher: CoroutineDispatcher,
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher
) : PagingDataAdapter<UiModel, RecyclerView.ViewHolder>(
    diffCallback = PhotoUiModelComparator,
    mainDispatcher = mainDispatcher,
    workerDispatcher = ioDispatcher
) {
    internal var clickListener: PhotoClickListener = object : PhotoClickListener {
        override fun onClick(model: UiModel) = Unit
    }

    override fun getItemViewType(position: Int): Int {
        return when (val item = getItem(position)) {
            is UiModel.SeparatorUiModel -> R.layout.row_photo_separtor
            is UiModel.PhotoUiModel -> R.layout.row_photo_thumb
            else -> R.layout.row_photo_separtor
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        when (viewType) {
            R.layout.row_photo_thumb -> PhotoViewHolder(
                binding = RowPhotoThumbBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                ),
                listener = clickListener
            )
            R.layout.row_photo_separtor -> SeparatorViewHolder(
                binding = RowPhotoSepartorBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
            else -> throw IllegalStateException("Unknown view type")
        }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        getItem(position) ?: return

        when (holder) {
            is PhotoViewHolder -> holder.bind(getItem(position) as UiModel.PhotoUiModel)
            is SeparatorViewHolder ->
                holder.bindPlaceholder(getItem(position) as UiModel.SeparatorUiModel)
        }
    }
}

class EmptyClickListener : PhotoClickListener {
    override fun onClick(model: UiModel) {}
}

interface PhotoClickListener {
    fun onClick(model: UiModel)
}

class PhotoViewHolder(
    private val binding: RowPhotoThumbBinding,
    private val listener: PhotoClickListener
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(model: UiModel.PhotoUiModel) {
        Glide.with(binding.root.context)
            .load(model.content)
            .placeholder(android.R.drawable.ic_lock_lock)
            .centerCrop()
            .transition(withCrossFade(300))
            .thumbnail(.2f)
            .into(binding.photoThumb)

        binding.apply {
            this.model = model
            this.listener = this@PhotoViewHolder.listener
            executePendingBindings()
        }
    }
}

class SeparatorViewHolder(
    private val binding: RowPhotoSepartorBinding
) : RecyclerView.ViewHolder(binding.root) {
    fun bindPlaceholder(model: UiModel.SeparatorUiModel) {
        binding.apply {
            text.text = model.text
            executePendingBindings()
        }
    }
}

object PhotoUiModelComparator : DiffUtil.ItemCallback<UiModel>() {
    override fun areItemsTheSame(oldItem: UiModel, newItem: UiModel): Boolean {
        val isSamePhotoItem =
            oldItem is UiModel.PhotoUiModel
                    && newItem is UiModel.PhotoUiModel
                    && newItem.id == oldItem.id

        val isSameSeparator =
            oldItem is UiModel.SeparatorUiModel
                    && newItem is UiModel.SeparatorUiModel
                    && newItem.text == oldItem.text

        return isSamePhotoItem || isSameSeparator
    }

    override fun areContentsTheSame(oldItem: UiModel, newItem: UiModel): Boolean =
        oldItem == newItem
}
