/*
 * Copyright 2020 cyberfikcja.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.cyberfikcja.safephotos.ui

import android.graphics.Bitmap
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.channelFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.flattenMerge
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.internal.ChannelFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEmpty
import kotlinx.coroutines.flow.scan
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import net.cyberfikcja.safephotos.core.FlowUseCase
import net.cyberfikcja.safephotos.core.MainDispatcher
import net.cyberfikcja.safephotos.data.model.Password
import net.cyberfikcja.safephotos.domain.LoadPhotosUseCase
import net.cyberfikcja.safephotos.domain.LoadPhotosUseCase.LoadPhotosAction
import net.cyberfikcja.safephotos.domain.PhotoState
import net.cyberfikcja.safephotos.domain.SavePhotoUseCase
import net.cyberfikcja.safephotos.domain.UserRepository
import net.cyberfikcja.safephotos.domain.UserSession
import timber.log.Timber
import javax.inject.Inject

sealed class PartialViewState {
    object Other : PartialViewState()
    object UserLoggedIn : PartialViewState()
    object UserLoggedOut : PartialViewState()
    object Loading : PartialViewState()
    object Empty : PartialViewState()
    data class PhotosLoaded(val pagingData: Flow<PagingData<PhotoState>>) : PartialViewState()
    data class PhotoSaved(val id: Long) : PartialViewState()
    data class LoadError(val error: Throwable) : PartialViewState()
    data class PhotoSaveError(val error: Throwable) : PartialViewState()
    data class AuthError(val error: Throwable) : PartialViewState()
}

data class PhotosViewState(
    val authenticated: Boolean = false,
    val content: Flow<PagingData<PhotoState>> = flowOf(),
    val loading: Boolean = false,
    val error: Throwable? = null
)

sealed class UiModel {
    data class PhotoUiModel(val id: Long, val content: Bitmap) : UiModel()
    data class SeparatorUiModel(val text: String) : UiModel()
}

sealed class NavEvent
object ShowSignInDialog : NavEvent()

sealed class UiEvent {
    object Init : UiEvent()
    object Start : UiEvent()
    object Stop : UiEvent()
    data class SavePhoto(val bitmap: Bitmap) : UiEvent()
    data class Authenticate(val password: Password) : UiEvent()
    data class LoadPhotos(val pageSize: Int) : UiEvent()
}

class SaveUserSessionUseCase @Inject constructor(
    @MainDispatcher private val dispatcher: CoroutineDispatcher,
    private val userRepository: UserRepository
) : FlowUseCase<Password, UserSession>(dispatcher) {
    override fun execute(parameters: Password): Flow<Result<UserSession>> {
        return channelFlow {
            userRepository.save(parameters)
                .map { Result.success(it) }
        }
    }
}

class PhotosViewModel @ViewModelInject constructor(
    private val loadPhotosUseCase: LoadPhotosUseCase,
    private val savePhotoUseCase: SavePhotoUseCase,
    private val signInUseCase: SaveUserSessionUseCase
) : ViewModel() {

    private val _viewState = MutableStateFlow(PhotosViewState())
    val viewState: StateFlow<PhotosViewState> = _viewState.asStateFlow()

    private val _events = MutableStateFlow<UiEvent>(UiEvent.Init)
    private val _eventsInternal: Flow<PhotosViewState> = _events
        .map {
            Timber.d("event [$it]")
            when (it) {
                is UiEvent.Authenticate -> authenticate(password = it.password)
                is UiEvent.SavePhoto -> savePhoto(bitmap = it.bitmap)
                is UiEvent.LoadPhotos -> loadPhotos(pageSize = it.pageSize)
                else -> flowOf(PartialViewState.Other)
            }
        }
        .flattenMerge()
        .scan(_viewState.value, { state, partial ->
            Timber.d("state [$state] \npartial [${partial}]")
            when (partial) {
                PartialViewState.Other -> state
                PartialViewState.UserLoggedIn -> state.copy(authenticated = true)
                    .also { _events.emit(UiEvent.LoadPhotos(50)) }
                PartialViewState.UserLoggedOut -> PhotosViewState()
                PartialViewState.Loading -> PhotosViewState(loading = true)
                PartialViewState.Empty -> state
                is PartialViewState.PhotosLoaded -> state.copy(content = partial.pagingData)
                is PartialViewState.PhotoSaved -> state.copy(loading = false)
                is PartialViewState.LoadError -> state.copy(
                    loading = false,
                    error = partial.error
                )
                is PartialViewState.PhotoSaveError -> state.copy(
                    loading = false,
                    error = partial.error
                )
                is PartialViewState.AuthError -> state.copy(
                    loading = false,
                    error = partial.error
                )
            }
        })

    init {
        viewModelScope.launch {
            _eventsInternal.stateIn(viewModelScope).collectLatest(::render)
        }
    }

    fun onEvent(uiEvent: UiEvent) {
        viewModelScope.launch {
            _events.emit(uiEvent)
        }
    }

    private suspend fun render(photosViewState: PhotosViewState) {
        Timber.d("render $photosViewState")
        _viewState.emit(photosViewState)
    }

    private suspend fun loadPhotos(pageSize: Int): Flow<PartialViewState> =
        loadPhotosUseCase(LoadPhotosUseCase.Params(pageSize = pageSize, scope = viewModelScope))
            .map { it.getOrThrow() }
            .map {
                when (it) {
                    is LoadPhotosAction.Content -> PartialViewState.PhotosLoaded(pagingData = it.pagingData)
                }
            }
            .also { it.stateIn(viewModelScope) }

    private suspend fun savePhoto(bitmap: Bitmap): Flow<PartialViewState> =
        savePhotoUseCase(PhotoState.Photo(bitmap = bitmap))
            .map { it.getOrThrow() }
            .catch { Result.failure<PartialViewState>(it) }
            .map { PartialViewState.PhotoSaved(it) }
            .also { it.stateIn(viewModelScope) }

    private suspend fun authenticate(password: Password): Flow<PartialViewState> =
        signInUseCase(password)
            .map { it.getOrThrow() }
            .catch { emit(UserSession.InActive) }
            .onEmpty { emit(UserSession.InActive) }
            .map { PartialViewState.UserLoggedIn }
            .also { it.stateIn(viewModelScope) }
}
