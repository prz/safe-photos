/*
 * Copyright 2020 cyberfikcja.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.cyberfikcja.safephotos.ui

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import androidx.activity.viewModels
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.lifecycleScope
import androidx.paging.CombinedLoadStates
import androidx.paging.LoadState
import androidx.paging.PagingData
import androidx.paging.map
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import net.cyberfikcja.safephotos.R
import net.cyberfikcja.safephotos.data.model.Password
import net.cyberfikcja.safephotos.databinding.ActivityPhotosBinding
import net.cyberfikcja.safephotos.domain.PhotoState
import timber.log.Timber
import javax.inject.Inject

@AndroidEntryPoint
class PhotosActivity : AppCompatActivity() {

    @Inject
    internal lateinit var photosAdapter: PhotosAdapter

    private lateinit var binding: ActivityPhotosBinding

    private val viewModel: PhotosViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView<ActivityPhotosBinding>(
            this,
            R.layout.activity_photos
        ).apply {
            lifecycleOwner = this@PhotosActivity
        }

        binding.toolbar.inflateMenu(R.menu.toolbar_list_menu)
        binding.toolbar.setOnMenuItemClickListener {
            onMenuItemClicked(it)
        }

        binding.photoList.apply {
            layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
            adapter = photosAdapter
        }

        photosAdapter.clickListener = object : PhotoClickListener {
            override fun onClick(model: UiModel) {
                Timber.i("click [$model]")
            }
        }

        lifecycleScope.launchWhenStarted {
            viewModel.viewState.collectLatest(this@PhotosActivity::bind)
        }

        photosAdapter.addLoadStateListener {
            Timber.d("Combined load state $it")
        }
    }

    private suspend fun bind(state: PhotosViewState) {
        Timber.d("state [$state]")
        state.error?.let { showSnackbar(R.string.error_something_went_wrong) }
        state.authenticated.takeIf { it } ?: showSignInDialog()

        lifecycleScope.launch {
            state.content.collectLatest { pagingData ->
                val data: PagingData<UiModel> = pagingData
                    .map { model ->
                        when (model) {
                            is PhotoState.Separator -> UiModel.SeparatorUiModel(
                                text ="Loading..."
                            )
                            is PhotoState.Photo -> UiModel.PhotoUiModel(
                                id = model.id,
                                content = model.bitmap
                            )
                        }
                    }
                photosAdapter.submitData(data)
                Timber.d("Snapshot size ${photosAdapter.snapshot().size}")
            }
        }
    }

    override fun onStart() {
        super.onStart()
        viewModel.onEvent(UiEvent.Start)
        photosAdapter.refresh()
    }

    override fun onStop() {
        super.onStop()
        viewModel.onEvent(UiEvent.Stop)
    }

    private fun onMenuItemClicked(menuItem: MenuItem?): Boolean {
        when (menuItem?.itemId) {
            R.id.menu_list_take_photo -> launchTakePhotoWithPreview { bitmap ->
                bitmap?.let { viewModel.onEvent(UiEvent.SavePhoto(bitmap)) }
                    ?: Timber.d("User backed out from taking photo")
            }
        }
        return true
    }


    private fun showSnackbar(@StringRes messageRes: Int) {
        Snackbar.make(binding.photoThumbContainer, messageRes, Snackbar.LENGTH_LONG).show()
    }

    private fun showSignInDialog() {
        Timber.i("Show sign in dialog")
        val view = View.inflate(this, R.layout.dialog_password_layout, null)
        val editTextView: EditText = view.findViewById(R.id.password_input_edit_text)
        val onPositiveButtonClicked: (CharArray) -> Unit =
            { viewModel.onEvent(UiEvent.Authenticate(Password(it))) }
        val onNegativeButtonClicked: () -> Unit = { finish() }

        MaterialAlertDialogBuilder(this)
            .setTitle(R.string.dialog_password_title)
            .setView(view)
            .setPositiveButton(R.string.dialog_password_positive_button) { _, _ ->
                onPositiveButtonClicked(editTextView.text.toCharArray())
            }
            .setNegativeButton(R.string.dialog_password_negative_button) { _, _ -> onNegativeButtonClicked() }
            .setCancelable(false)
            .create()
            .show()
    }
}
