Safe Photos app
=================

The app allows to take photos and securely store them in device memory. Upon launch user is asked for the password, which must match with the one stored in app's resources.

## Why
The app shows a practical solution for storing and encrypting data, demonstrates approach on clean architecture and related concepts with usages of Android Jetpack.

## Cryptography
As per functional requirements, photos must be encrypted with either a key derived from user password or password should just protect the encryption key. Easy... kind of. But it depends on couple factors. Let's analyse possible solutions.

> a key derived from user password (...)

Public/Private key encryption is ruled out by a need of having a certificate chain, which is out of scope.

The 256-bit symmetric AES cipher in GCM mode can be used with Password Based Encryption algorithm and Password-Based Key Derivation Function 2 (PBKDF2). It requires to generate and store salt-secret, which iterated over user password can generate the `SecretKey` - used for both encryption and decryption. Depending on Android minimum SDK level, we can choose from variety of parameters for message digest and mac. Encrypted photos can be stored in app's database.

> (...) or password should just protect the encryption key

Android Jetpack has released `security-crypto` library (currently `1.0.0-rc03` version). It offers two components: `EncryptedSharedPreferences` and `EncryptedFile`. Cryptography in both cases is being handled by system process (unlike password-derived key solution) and stores master key in Android `KeyStore`. This requires API level 23+. The user password can be stored in `EncryptedSharedPreferences` and used only for authorization. Then photos would be encrypted with `EncryptedFile` and stored in app's file directory.

Both solutions have their drawbacks. `security-crypto` is not stable and allows only devices with Android 23+. Password-derived key approach requires storing the random secret for further operations and performs cryptography in the application process.

In order to match security standards, currently password-derived key solution with 256-bit AES/GCM and `PBKDF2WithHmacSHA512` algorithm is being used - this allows to perform cryptographic operations on devices with API level 26+. Device range can be extended by using e.g. `PBKDF2withHmacSHA1`, for devices with API 10+. Though this algorithm may not comply with security standards, i.e. [NSA Suite B](https://en.wikipedia.org/wiki/NSA_Suite_B_Cryptography)

## Storage
Secret random key (salt) is being stored in `EncryptedSharedPreferences`. Encrypted photos are stored in Room database. After decryption, photos are only held in memory.

## UI
Material design components has been used.

## Architecture
This app implements [Clean Architecture](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html), applies domain-driven design concepts, i.e. unidirectional data flow and model immutability. See children of `UseCase<I,O>` for details on business logic. For presentation layer, MVVM and data binding is used.

    NOTE: This app strongly relies on Kotlin Coroutines and dependency injection using Hilt.

## Next steps
It's always better to use ready cryptography protocols offered by platform/framework, rather then implementing them. Once `security-crypto` is stable, switching purely to this solution shall be considered.

## Feedback
Please don't hesitate and share any feedback. This will be greatly appreciated.

## Copyright

    Copyright 2020 Cyberfikcja. All rights reserved.

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
