/*
 * Copyright 2020 cyberfikcja.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

plugins {
    id("java-platform")
}

val appcompat = "1.2.0"
val activity = "1.2.0-alpha04"
val cardview = "1.0.0"
val archTesting = "2.0.0"
val benchmark = "1.0.0"
val constraintLayout = "1.1.3"
val core = "1.2.0"
val coroutines = "1.4.2"
val coroutinesTest = "1.4.2"
val drawerLayout = "1.1.0-rc01"
val espresso = "3.1.1"
val fragment = "1.2.4"
val glide = "4.9.0"
val gson = "2.8.6"
val hamcrest = "1.3"
val hilt = Versions.HILT
val hiltJetPack = "1.0.0-alpha02"
val junit = "4.13"
val junitExt = "1.1.1"
val lifecycle = "2.2.0"
val material = "1.2.0"
val mockito = "3.3.1"
val mockitoKotlin = "1.5.0"
val okhttp = "3.10.0"
val okio = "1.14.0"
val paging = "3.0.0-alpha09"
val retrofit = "2.9.0"
val room = "2.3.0-alpha03"
val rules = "1.1.1"
val runner = "1.2.0"
val securityCrypto = "1.0.0-rc03"
val timber = "4.7.1"
val viewpager2 = "1.0.0"

dependencies {
    constraints {
        api("${Libraries.ACTIVITY_KTX}:$activity")
        api("${Libraries.ANDROIDX_HILT_COMPILER}:$hiltJetPack")
        api("${Libraries.APPCOMPAT}:$appcompat")
        api("${Libraries.CARDVIEW}:$cardview")
        api("${Libraries.ARCH_TESTING}:$archTesting")
        api("${Libraries.BENCHMARK}:$benchmark")
        api("${Libraries.CONSTRAINT_LAYOUT}:$constraintLayout")
        api("${Libraries.CORE_KTX}:$core")
        api("${Libraries.COROUTINES}:$coroutines")
        api("${Libraries.COROUTINES_DEBUG}:$coroutines")
        api("${Libraries.COROUTINES_TEST}:$coroutines")
        api("${Libraries.DRAWER_LAYOUT}:$drawerLayout")
        api("${Libraries.ESPRESSO_CORE}:$espresso")
        api("${Libraries.ESPRESSO_CONTRIB}:$espresso")
        api("${Libraries.FRAGMENT_KTX}:$fragment")
        api("${Libraries.GLIDE}:$glide")
        api("${Libraries.GLIDE_COMPILER}:$glide")
        api("${Libraries.GSON}:$gson")
        api("${Libraries.HAMCREST}:$hamcrest")
        api("${Libraries.HILT_ANDROID}:$hilt")
        api("${Libraries.HILT_COMPILER}:$hilt")
        api("${Libraries.HILT_TESTING}:$hilt")
        api("${Libraries.HILT_VIEWMODEL}:$hiltJetPack")
        api("${Libraries.JUNIT}:$junit")
        api("${Libraries.EXT_JUNIT}:$junitExt")
        api("${Libraries.KOTLIN_STDLIB}:${Versions.KOTLIN}")
        api("${Libraries.LIFECYCLE_COMPILER}:$lifecycle")
        api("${Libraries.LIFECYCLE_LIVE_DATA_KTX}:$lifecycle")
        api("${Libraries.LIFECYCLE_VIEW_MODEL_KTX}:$lifecycle")
        api("${Libraries.MATERIAL}:$material")
        api("${Libraries.MOCKITO_CORE}:$mockito")
        api("${Libraries.MOCKITO_KOTLIN}:$mockitoKotlin")
        api("${Libraries.RETROFIT}:$retrofit")
        api("${Libraries.RETROFIT_CONVERTER_GSON}:$retrofit")
        api("${Libraries.ROOM_KTX}:$room")
        api("${Libraries.ROOM_RUNTIME}:$room")
        api("${Libraries.ROOM_COMPILER}:$room")
        api("${Libraries.OKHTTP}:$okhttp")
        api("${Libraries.OKHTTP_LOGGING_INTERCEPTOR}:$okhttp")
        api("${Libraries.OKIO}:$okio")
        api("${Libraries.PAGING_COMMON_KTX}:$paging")
        api("${Libraries.PAGING_RUNTIME_KTX}:$paging")
        api("${Libraries.PAGING_RX_KTX}:$paging")
        api("${Libraries.RULES}:$rules")
        api("${Libraries.RUNNER}:$runner")
        api("${Libraries.SECURITY_CRYPTO}:$securityCrypto")
        api("${Libraries.TIMBER}:$timber")
        api("${Libraries.VIEWPAGER2}:$viewpager2")
    }
}
