/*
 * Copyright 2020 Cyberfikcja
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

object Libraries {
    const val ACTIVITY_KTX = "androidx.activity:activity-ktx"
    const val ANDROIDX_HILT_COMPILER = "androidx.hilt:hilt-compiler"
    const val APPCOMPAT = "androidx.appcompat:appcompat"
    const val ARCH_TESTING = "androidx.arch.core:core-testing"
    const val BENCHMARK = "androidx.benchmark:benchmark-junit4"
    const val CARDVIEW = "androidx.cardview:cardview"
    const val CONSTRAINT_LAYOUT = "androidx.constraintlayout:constraintlayout"
    const val CORE_KTX = "androidx.core:core-ktx"
    const val COROUTINES = "org.jetbrains.kotlinx:kotlinx-coroutines-core"
    const val COROUTINES_DEBUG = "org.jetbrains.kotlinx:kotlinx-coroutines-debug"
    const val COROUTINES_TEST = "org.jetbrains.kotlinx:kotlinx-coroutines-test"
    const val DRAWER_LAYOUT = "androidx.drawerlayout:drawerlayout"
    const val ESPRESSO_CONTRIB = "androidx.test.espresso:espresso-contrib"
    const val ESPRESSO_CORE = "androidx.test.espresso:espresso-core"
    const val EXT_JUNIT = "androidx.test.ext:junit"
    const val FRAGMENT_KTX = "androidx.fragment:fragment-ktx"
    const val GLIDE = "com.github.bumptech.glide:glide"
    const val GLIDE_COMPILER = "com.github.bumptech.glide:compiler"
    const val GSON = "com.google.code.gson:gson"
    const val HAMCREST = "org.hamcrest:hamcrest-library"
    const val HILT_ANDROID = "com.google.dagger:hilt-android"
    const val HILT_VIEWMODEL = "androidx.hilt:hilt-lifecycle-viewmodel"
    const val HILT_COMPILER = "com.google.dagger:hilt-android-compiler"
    const val HILT_TESTING = "com.google.dagger:hilt-android-testing"
    const val JUNIT = "junit:junit"
    const val KOTLIN_STDLIB = "org.jetbrains.kotlin:kotlin-stdlib-jdk7"
    const val LIFECYCLE_COMPILER = "androidx.lifecycle:lifecycle-compiler"
    const val LIFECYCLE_LIVE_DATA_KTX = "androidx.lifecycle:lifecycle-livedata-ktx"
    const val LIFECYCLE_VIEW_MODEL_KTX = "androidx.lifecycle:lifecycle-viewmodel-ktx"
    const val MATERIAL = "com.google.android.material:material"
    const val MOCKITO_CORE = "org.mockito:mockito-core"
    const val MOCKITO_KOTLIN = "com.nhaarman:mockito-kotlin"
    const val OKHTTP = "com.squareup.okhttp3:okhttp"
    const val OKHTTP_LOGGING_INTERCEPTOR = "com.squareup.okhttp3:logging-interceptor"
    const val OKIO = "com.squareup.okio:okio"
    const val PAGING_RUNTIME_KTX = "androidx.paging:paging-runtime-ktx"
    const val PAGING_COMMON_KTX = "androidx.paging:paging-common-ktx"
    const val PAGING_RX_KTX = "androidx.paging:paging-rxjava2-ktx"
    const val RETROFIT = "com.squareup.retrofit2:retrofit"
    const val RETROFIT_CONVERTER_GSON = "com.squareup.retrofit2:converter-gson"
    const val ROOM_COMPILER = "androidx.room:room-compiler"
    const val ROOM_KTX = "androidx.room:room-ktx"
    const val ROOM_RUNTIME = "androidx.room:room-runtime"
    const val RULES = "androidx.test:rules"
    const val RUNNER = "androidx.test:runner"
    const val SECURITY_CRYPTO = "androidx.security:security-crypto"
    const val TIMBER = "com.jakewharton.timber:timber"
    const val VIEWPAGER2 = "androidx.viewpager2:viewpager2"
}
